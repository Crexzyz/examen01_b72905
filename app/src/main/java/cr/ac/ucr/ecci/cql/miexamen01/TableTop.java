package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class TableTop implements Parcelable
{
    private String id;
    private String name;
    private int year;
    private String publisher;
    private String country;
    private double latitude;
    private double longitude;
    private String description;
    private String playersNumber;
    private String ages;
    private String playingTime;
    private int geoIdFK;

    /**
     * Constructor para utilizar solamente esta clase en el paso de parámetros.
     * En vez de pasar también la de Geographics y hacer "joins" en Java
     */
    public TableTop(String id, String name, int year, String publisher,
                    String country, double latitude, double longitude,
                    String description, String playersNumber, String ages,
                    String playingTime, int geoIdFK)
    {
        this.setId(id);
        this.setName(name);
        this.setYear(year);
        this.setPublisher(publisher);
        this.setCountry(country);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setDescription(description);
        this.setPlayersNumber(playersNumber);
        this.setAges(ages);
        this.setPlayingTime(playingTime);
        this.setGeoIdFK(geoIdFK);
    }

    /**
     * Constructor que se usa para la inserción de datos en la tabla
     * No tiene los campos que se fueron a Geographics con el refactoring
     */
    public TableTop(String id, String name, int year, String publisher,
                    String description, String playersNumber, String ages,
                    String playingTime, int geoIdFK)
    {
        this.setId(id);
        this.setName(name);
        this.setYear(year);
        this.setPublisher(publisher);
        this.setCountry("");
        this.setLatitude(0.0);
        this.setLongitude(0.0);
        this.setDescription(description);
        this.setPlayersNumber(playersNumber);
        this.setAges(ages);
        this.setPlayingTime(playingTime);
        this.setGeoIdFK(geoIdFK);
    }

    public TableTop(Cursor cursor)
    {
        this.setData(cursor);
    }

    protected TableTop(Parcel in) {
        id = in.readString();
        name = in.readString();
        year = in.readInt();
        publisher = in.readString();
        country = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        description = in.readString();
        playersNumber = in.readString();
        ages = in.readString();
        playingTime = in.readString();
        geoIdFK = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeInt(year);
        dest.writeString(publisher);
        dest.writeString(country);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
        dest.writeString(playersNumber);
        dest.writeString(ages);
        dest.writeString(playingTime);
        dest.writeInt(geoIdFK);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    /**
     * Consigue la base de datos de escritura y llama al insert con la base
     */
    public void insert(Context context)
    {
        SQLiteDatabase db = TestDatabase.getInstance(context).getWritableDatabase();
        this.insert(db);
    }

    /**
     * Inserción real de datos. Con el refactoring inserta una llave foránea para
     * los datos geográficos
     *
     */
    public long insert(SQLiteDatabase db)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.DatabaseEntry._ID, this.getId());
        values.put(DatabaseContract.DatabaseEntry.C_NAME, this.getName());
        values.put(DatabaseContract.DatabaseEntry.C_YEAR, this.getYear());
        values.put(DatabaseContract.DatabaseEntry.C_PUBLISHER, this.getPublisher());
        values.put(DatabaseContract.DatabaseEntry.C_DESCRIPTION, this.getDescription());
        values.put(DatabaseContract.DatabaseEntry.C_PLAYERS_NUMBER, this.getPlayersNumber());
        values.put(DatabaseContract.DatabaseEntry.C_AGES, this.getAges());
        values.put(DatabaseContract.DatabaseEntry.C_PLAYING_TIME, this.getPlayingTime());
        values.put(DatabaseContract.DatabaseEntry.C_GEO_ID_FK, this.getGeoIdFK());

        return db.insert(DatabaseContract.DatabaseEntry.T_TABLE_TOP, null, values);
    }

    public static void insertAll(Context context)
    {
        // Se recrea para insertar los datos sin problemas de llave primaria
        TestDatabase db = TestDatabase.getInstance(context);
        db.initializeDatabase();

        // Creación de objetos usando el constructor apropiado para la clase TableTop
        // Nótese el 2 repetido al final en TT002 y TT004 como parte del refactoring
        TableTop[] countries = {
            new TableTop("TT001", "Catan", 1995, "Kosmos",
                    "Picture yourself in the era of discoveries: after a long voyage of great deprivation, your ships have finally reached the coast of an uncharted island. Its name shall be Catan! But you are not the only discoverer. Other fearless seafarers have also landed on the shores of Catan: the race to settle the island has begun!",
                    "3-4", "10+", "1-2 hours", 1),
            new TableTop("TT002", "Monopoly", 1935, "Hasbro",
                    "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel! The more properties each player owns, the more rent can be charged. Chance cards could be worth money, or one might just say Go To Jail!",
                    "2-8", "8+", "20-180 minutes", 2),
            new TableTop("TT003", "Eldritch Horror", 2013, "Fantasy Flight Games",
                    "An ancient evil is stirring. You are part of a team of unlikely heroes engaged in an international struggle to stop the gathering darkness. To do so, you’ll have to defeat foul monsters, travel to Other Worlds, and solve obscure mysteries surrounding this unspeakable horror. The effort may drain your sanity and cripple your body, but if you fail, the Ancient One will awaken and rain doom upon the known world. ",
                    "1-8", "14+", "2-4 hours", 3),
            new TableTop("TT004", "Magic: the Gathering", 1993, "Hasbro",
                    "Magic: The Gathering is a collectible and digital collectible card game created by Richard Garfield. Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts, and summon creatures as depicted on individual cards in order to defeat their opponents, typically, but not always, by draining them of their 20 starting life points in the standard format.",
                    "2+", "13+", "Varies", 2),
            new TableTop("TT005", "Hanabi",  2010, "Asmodee",
                    "Hanabi—named for the Japanese word for \"fireworks\"—is a cooperative game in which players try to create the perfect fireworks show by placing the cards on the table in the right order.",
                    "2-5", "8+", "25 minutes", 4)
        };

        Geographics[] geos = {
                new Geographics(1, "Germany", 48.774538, 9.188467),
                new Geographics(2, "United States", 41.883736, -71.352259),
                new Geographics(3, "United States", 45.015417, -93.183995),
                new Geographics(4, "France", 48.761629, 2.065296)
        };

        for(Geographics geo : geos)
            geo.insert(context);

        for(TableTop tableTop : countries)
            tableTop.insert(context);
    }

    /**
     * Consigue todos los juegos TableTop y los devuelve en una lista para que los maneje el
     * adapter
     */
    public static List<TableTop> getAll(Context context)
    {
        SQLiteDatabase db = TestDatabase.getInstance(context).getReadableDatabase();

        // Query con joins como parte del refactoring
        final String JOIN_QUERY =
            "SELECT * FROM "
            + DatabaseContract.DatabaseEntry.T_TABLE_TOP + " t "
            + " INNER JOIN "
            + DatabaseContract.DatabaseEntry.T_GEOGRAPHICS + " g "
            + "ON  t.geo_id = g.gid;";

        // Se ejecuta la consulta
        Cursor cursor = db.rawQuery(JOIN_QUERY, null);

        // Por cada resultado se crea un objeto TableTop "Completo"
        // (ver comentarios del constructor con el cursor como parámetro)
        if (cursor.moveToFirst())
        {
            ArrayList<TableTop> list = new ArrayList<>();
            while (!cursor.isAfterLast())
            {
                TableTop tableTop = new TableTop(cursor);
                list.add(tableTop);
                cursor.moveToNext();
            }

            return list;
        }

        return null;
    }

    /**
     * Asigna datos al objeto basándose en lo que viene en un cursor
     */
    private void setData(Cursor cursor)
    {
        this.setId(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry._ID)));
        this.setName(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_NAME)));
        this.setYear(cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_YEAR)));
        this.setPublisher(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_PUBLISHER)));
        this.setCountry(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_COUNTRY)));
        this.setLatitude(cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_LATITUDE)));
        this.setLongitude(cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_LONGITUDE)));
        this.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_DESCRIPTION)));
        this.setPlayersNumber(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_PLAYERS_NUMBER)));
        this.setAges(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_AGES)));
        this.setPlayingTime(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_PLAYING_TIME)));

        // No es necesario setear este atributo del t_odo, no se necesita para las funciones de interfaz
        this.setGeoIdFK(cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DatabaseEntry.C_GEO_ID_FK)));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlayersNumber() {
        return playersNumber;
    }

    public void setPlayersNumber(String playersNumber) {
        this.playersNumber = playersNumber;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGeoIdFK() {
        return geoIdFK;
    }

    public void setGeoIdFK(int geoIdFK) {
        this.geoIdFK = geoIdFK;
    }
}
