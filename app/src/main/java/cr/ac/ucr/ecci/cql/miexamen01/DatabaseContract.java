package cr.ac.ucr.ecci.cql.miexamen01;

import android.provider.BaseColumns;

/**
 * Contrato de la base de datos
 * Contiene las tablas refactorizadas para evitar redundancia de datos en la parte geográfica
 * Se metió el país y coordenadas en otra tabla como parte del refactoring
 */
public final class DatabaseContract
{

    private DatabaseContract() {}

    public static class DatabaseEntry implements BaseColumns
    {
        public static final String T_TABLE_TOP = "table_top";
        public static final String _ID = "id";
        public static final String C_NAME = "name";
        public static final String C_YEAR = "year";
        public static final String C_PUBLISHER = "publisher";
        public static final String C_DESCRIPTION = "description";
        public static final String C_PLAYERS_NUMBER = "players_number";
        public static final String C_AGES = "AGES";
        public static final String C_PLAYING_TIME = "playing_time";
        public static final String C_GEO_ID_FK = "geo_id";

        public static final String T_GEOGRAPHICS = "geographics";
        public static final String C_GEO_ID = "gid"; // No se recicla el _ID porque dan problemas al obtener datos del cursor
        public static final String C_COUNTRY = "country";
        public static final String C_LATITUDE = "latitude";
        public static final String C_LONGITUDE = "longitude";
    }

    private static final String TEXT_T = " TEXT ";
    private static final String INTEGER_T = " INTEGER ";
    private static final String REAL_T = " REAL ";
    private static final String SEP = ",";
    private static final String CREATE_TABLE = "CREATE TABLE ";
    private static final String PK = " PRIMARY KEY ";
    private static final String FK = " FOREIGN KEY ";

    public static final String CREATE_T_TABLE_TOP = CREATE_TABLE
            + DatabaseEntry.T_TABLE_TOP + " ("
            + DatabaseEntry._ID + TEXT_T + PK + SEP
            + DatabaseEntry.C_NAME + TEXT_T + SEP
            + DatabaseEntry.C_YEAR + INTEGER_T + SEP
            + DatabaseEntry.C_GEO_ID_FK + INTEGER_T + SEP
            + DatabaseEntry.C_PUBLISHER + TEXT_T + SEP
            + DatabaseEntry.C_DESCRIPTION + TEXT_T + SEP
            + DatabaseEntry.C_PLAYERS_NUMBER + TEXT_T + SEP
            + DatabaseEntry.C_AGES + TEXT_T + SEP
            + DatabaseEntry.C_PLAYING_TIME + TEXT_T + SEP
            + FK + "(" + DatabaseEntry.C_GEO_ID_FK + ")" + " REFERENCES "
            + DatabaseEntry.T_GEOGRAPHICS + "(" + DatabaseEntry._ID + ")" + " );";


    public static final String DROP_T_TABLE_TOP =
            "DROP TABLE IF EXISTS " + DatabaseEntry.T_TABLE_TOP;

    public static final String CREATE_T_GEOGRAPHICS = CREATE_TABLE
            + DatabaseEntry.T_GEOGRAPHICS + " ("
            + DatabaseEntry.C_GEO_ID + INTEGER_T + PK + SEP
            + DatabaseEntry.C_COUNTRY + TEXT_T + SEP
            + DatabaseEntry.C_LATITUDE + REAL_T + SEP
            + DatabaseEntry.C_LONGITUDE + REAL_T + " );";

    public static final String DROP_T_GEOGRAPHICS =
            "DROP TABLE IF EXISTS " + DatabaseEntry.T_GEOGRAPHICS;
}
