package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private static final int LOCATION_REQUEST_CODE = 101;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        // Toma los datos enviados desde el fragmento como un bundle
        latitude = Objects.requireNonNull(getIntent().getExtras()).getDouble("LATITUDE");
        longitude = Objects.requireNonNull(getIntent().getExtras()).getDouble("LONGITUDE");
        name = Objects.requireNonNull(getIntent().getExtras()).getString("NAME");

        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Solicitar permisos para la localizacion
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // habilitamos los permisos para localizacion
            mMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions (this,
                    new String []{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }

        this.positionCamera();
    }

    private void positionCamera() {
        LatLng target = new LatLng(this.latitude, this.longitude);
        mMap.addMarker(new MarkerOptions().position(target).title(this.name));

        mMap.getUiSettings().setZoomControlsEnabled(true);

        // Mover la camara, zoom de largo para ver bien el país y orientación normal
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(target)
                .zoom(0) // zoom level
//                .bearing(70) // bearing // direccion de la camara
//                .tilt(45) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
