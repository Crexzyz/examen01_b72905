package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helper de la base de datos. Versión 4 producto del refactoring
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    public static final int DATABASE_VERSION = 4;

    public static final String DATABASE_NAME = "MiExamen01.db";

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void deleteAll(SQLiteDatabase db)
    {
        db.execSQL(DatabaseContract.DROP_T_TABLE_TOP);
        db.execSQL(DatabaseContract.DROP_T_GEOGRAPHICS);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DatabaseContract.CREATE_T_TABLE_TOP);
        db.execSQL(DatabaseContract.CREATE_T_GEOGRAPHICS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        deleteAll(db);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db, oldVersion, newVersion);
    }
}
