package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Contiene datos geográficos que utilizan los TableTop
 * clase creada como parte del refactoring
 * No se implementa el Parcelable ya que se manejan los datos de aquí
 * en la clase TableTop. Por facilidad de paso de parámetros por intents/bundles y evitar
 * hacer Joins en Java con los datos
 */
public class Geographics
{
    private int id;
    private String country;
    private double latitude;
    private double longitude;

    public Geographics(int id, String country, double latitude, double longitude)
    {
        this.setId(id);
        this.setCountry(country);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public void insert(Context context)
    {
        SQLiteDatabase db = TestDatabase.getInstance(context).getWritableDatabase();
        this.insert(db);
    }

    public void insert(SQLiteDatabase db)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.DatabaseEntry.C_GEO_ID, this.getId());
        values.put(DatabaseContract.DatabaseEntry.C_COUNTRY, this.getCountry());
        values.put(DatabaseContract.DatabaseEntry.C_LATITUDE, this.getLatitude());
        values.put(DatabaseContract.DatabaseEntry.C_LONGITUDE, this.getLongitude());

        db.insert(DatabaseContract.DatabaseEntry.T_GEOGRAPHICS, null, values);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
