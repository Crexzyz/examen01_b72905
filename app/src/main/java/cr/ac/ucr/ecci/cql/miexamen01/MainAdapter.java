package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Adapter de la lista de juegos
 * Basado y adaptado de
 * https://stackoverflow.com/questions/40584424/simple-android-recyclerview-example
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<TableTop> tables;
    private LayoutInflater inflater;
    private Context context;

    // data is passed into the constructor
    MainAdapter(Context context, List<TableTop> tables)
    {
        this.inflater = LayoutInflater.from(context);
        this.tables = tables;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.game_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the View in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TableTop table = tables.get(position);

        final String name = table.getId().toLowerCase();
        Resources resources = context.getResources();

        final int imgId = resources.getIdentifier(name, "drawable", context.getPackageName());

        holder.image.setImageResource(imgId);
        holder.name.setText(table.getName());
        holder.description.setText(String.valueOf(table.getDescription()));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return tables.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView name;
        TextView description;

        ViewHolder(View itemView)
        {
            super(itemView);
            image = itemView.findViewById(R.id.game_img);
            name = itemView.findViewById(R.id.game_name);
            description = itemView.findViewById(R.id.game_desc);
            itemView.setOnClickListener(this);
        }

        // Abre la actividad con detalle cuando se le hace click
        @Override
        public void onClick(View view)
        {
            TableTop tableTop = tables.get(getAdapterPosition());

            // Se agrega como "parámetro" el objeto al que se le hizo click
            Intent intent = new Intent(view.getContext(), DetailsActivity.class);
            intent.putExtra("TABLE", tableTop);

            view.getContext().startActivity(intent);
        }

    }
}