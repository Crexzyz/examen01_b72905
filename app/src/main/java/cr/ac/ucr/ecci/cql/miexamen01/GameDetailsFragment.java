package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;


/**
 * Fragmento con los detalles de un juego
 */
public class GameDetailsFragment extends Fragment {

    private TableTop tableTop = null;

    public GameDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Seteo de datos tomándolos del Parcelable al crear la actividad
     * Se usan strings con "expresiones regulares" para sustituir variables
     * en el string guardado en el xml
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // Obtener imágenes basándose en el id
        final String name = tableTop.getId().toLowerCase();
        Resources resources = this.getResources();
        final int imgId = resources.getIdentifier(name, "drawable", getActivity().getPackageName());

        ((ImageView)getActivity().findViewById(R.id.frg_image)).setImageResource(imgId);

        ((TextView)getActivity().findViewById(R.id.frg_name)).setText(tableTop.getName());
        ((TextView)getActivity().findViewById(R.id.frg_desc))
                .setText(String.format(tableTop.getDescription()));

        // Texto de año
        ((TextView)getActivity().findViewById(R.id.frg_year)).
                setText(String.format(resources.getString(R.string.year_text),
                        String.valueOf(tableTop.getYear())));

        // Texto del vendedor
        ((TextView)getActivity().findViewById(R.id.frg_publisher))
                .setText(String.format(resources.getString(R.string.publ_text),
                        String.valueOf(tableTop.getPublisher())));

        // Texto del país
        ((TextView)getActivity().findViewById(R.id.frg_country))
                .setText(String.format(resources.getString(R.string.country_text),
                        String.valueOf(tableTop.getCountry())));

        // Texto del número de jugadores
        ((TextView)getActivity().findViewById(R.id.frg_players_no))
                .setText(String.format(resources.getString(R.string.pl_no_text),
                        String.valueOf(tableTop.getPlayersNumber())));

        // Texto de las edades
        ((TextView)getActivity().findViewById(R.id.frg_ages))
                .setText(String.format(resources.getString(R.string.ages_text),
                        String.valueOf(tableTop.getAges())));

        // Texto del tiempo de juego
        ((TextView)getActivity().findViewById(R.id.frg_playing_time))
                .setText(String.format(resources.getString(R.string.play_time_text),
                        String.valueOf(tableTop.getPlayingTime())));

        this.initMapsButton();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Se toma el objeto TableTop enviado como extra. Aprovechando la
        // implementación de Parcelable
        this.tableTop = (TableTop)Objects.requireNonNull(getActivity()
                .getIntent().getExtras().getParcelable("TABLE"));

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_details, container, false);
    }

    /**
     * Agrega el listener para abrir la actividad de mapas y
     * adjunta los datos geográficos necesarios en el intent
     */
    private void initMapsButton()
    {
        Button button = getActivity().findViewById(R.id.maps_btn);

        final Context context = getContext();
        final TableTop tableTop = this.tableTop;

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MapsActivity.class);

                intent.putExtra("LATITUDE", tableTop.getLatitude());
                intent.putExtra("LONGITUDE", tableTop.getLongitude());
                intent.putExtra("NAME", tableTop.getName());

                startActivity(intent);
            }
        });
    }
}
