package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Clase singleton para obtener la base de datos e inicializarla
 */
public class TestDatabase
{

    private static TestDatabase INSTANCE;

    private DatabaseHelper helper;

    private Context context;

    // Constructor privado para tener solo una instancia
    private TestDatabase(Context context) { this.context = context; }

    // "Constructor" del modelo singleton
    public static TestDatabase getInstance(Context context)
    {
        if(INSTANCE == null)
        {
            INSTANCE = new TestDatabase(context);
        }

        return INSTANCE;
    }

    /**
     * Método de utilidad para reinsertar datos "quemados"
     */
    public void initializeDatabase()
    {
        helper = new DatabaseHelper(context);
        helper.getWritableDatabase().execSQL(DatabaseContract.DROP_T_TABLE_TOP);
        helper.getWritableDatabase().execSQL(DatabaseContract.DROP_T_GEOGRAPHICS);
        helper.getWritableDatabase().execSQL(DatabaseContract.CREATE_T_TABLE_TOP);
        helper.getWritableDatabase().execSQL(DatabaseContract.CREATE_T_GEOGRAPHICS);
    }

    public SQLiteDatabase getWritableDatabase()
    {
        return this.helper.getWritableDatabase();
    }

    public SQLiteDatabase getReadableDatabase()
    {
        return this.helper.getReadableDatabase();
    }

}
