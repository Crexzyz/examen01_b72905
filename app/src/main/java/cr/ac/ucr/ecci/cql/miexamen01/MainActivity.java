package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inserta/reinserta datos y los obtiene para dárselos al RecyclerView
        TableTop.insertAll(this);
        List<TableTop> list = TableTop.getAll(this);

        RecyclerView view = findViewById(R.id.main_rv);
        view.setLayoutManager(new LinearLayoutManager(this));

        view.setAdapter(new MainAdapter(this, list));

    }
}
