package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


/**
 * Actividad simple, la lógica se realiza principalmente en el fragmento
 */
public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }
}
